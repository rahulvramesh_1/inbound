from .models import Item ,Category ,Supplier
from table import Table
from table.columns import Column, LinkColumn, DatetimeColumn, Link, ImageLink
from table.utils import A


class ItemTable(Table):
    id = Column(field='id', header=u'#')
    title = Column(field='title', header=u'Product Title')
    stock_count = Column(field='stock_count', header=u'Available Stock')
    edit = LinkColumn(header=u'Edit', links=[
        Link(viewname='product:edit', args=(A('id'),),text='',attrs={'class':'fa fa-edit text-danger inline'})])
    delete = LinkColumn(header=u'Delete', links=[
        Link(viewname='product:delete_product', args=(A('id'),), text='', attrs={'class': 'fa fa-trash text-danger inline'})])
    class Meta:
        model = Item


class CategoryTable(Table):
    id = Column(field='id', header=u'#')
    cat_name = Column(field='cat_name', header=u'Category Name')
    cat_description = Column(field='cat_description', header=u'Description')
    edit = LinkColumn(header=u'Edit', links=[
        Link(viewname='product:edit_category', args=(A('id'),),text='',attrs={'class':'fa fa-edit text-danger inline'})])
    delete = LinkColumn(header=u'Delete', links=[
        Link(viewname='product:delete_category', args=(A('id'),), text='',
             attrs={'class': 'fa fa-trash text-danger inline'})])

    class Meta:
        model = Category


class SupplierTable(Table):
    id = Column(field='id', header=u'#')
    company_name = Column(field='company_name', header=u'Company Name')
    email = Column(field='email', header=u'Email')
    phone_number = Column(field='phone_number', header=u'Contact Number')
    comments = Column(field='comments', header=u'Comments')
    edit = LinkColumn(header=u'Edit', links=[
        Link(viewname='product:edit_supplier', args=(A('id'),),text='',attrs={'class':'fa fa-edit text-danger inline'})])
    delete = LinkColumn(header=u'Delete', links=[
        Link(viewname='product:delete_category', args=(A('id'),), text='',
             attrs={'class': 'fa fa-trash text-danger inline'})])

    class Meta:
        model = Supplier



