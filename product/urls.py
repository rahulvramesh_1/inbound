# log/urls.py
from django.conf.urls import url
from . import views

app_name = 'product'

# We are adding a URL called /home
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^create/$', views.create, name='create'),
    url(r'^manage/$', views.manage, name='manage'),
    url(r'^edit/(?P<pk>\d+)/$', views.edit, name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', views.delete_product, name='delete_product'),
    url(r'^category/$', views.create_category, name='create_category'),
    url(r'^category/manage/$', views.manage_category, name='manage_category'),
    url(r'^category/edit/(?P<pk>\d+)/$', views.edit_category, name='edit_category'),
    url(r'^category/delete/(?P<pk>\d+)/$', views.delete_category, name='delete_category'),
    url(r'^supplier/$', views.create_supplier, name='create_supplier'),
    url(r'^supplier/edit/(?P<pk>\d+)/$', views.edit_supplier, name='edit_supplier'),
    url(r'^supplier/manage/$', views.manage_supplier, name='manage_supplier'),
    url(r'^category/delete/(?P<pk>\d+)/$', views.delete_supplier, name='delete_supplier'),
]