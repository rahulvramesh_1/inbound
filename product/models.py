from django.db import models


# Create your models here.

class Category(models.Model):
    cat_name = models.CharField(max_length=30)
    cat_description = models.TextField()
    isActive = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.cat_name


class Supplier(models.Model):
    company_name = models.CharField(max_length=50)
    agency_name = models.CharField(max_length=50,null=True,blank=True)
    full_name = models.CharField(max_length=50)
    email = models.TextField(max_length=50)
    phone_number = models.TextField(max_length=20)
    address1 = models.TextField(max_length=50)
    address2 = models.TextField(max_length=50,null=True,blank=True)
    city = models.TextField(max_length=50)
    state = models.TextField(max_length=50)
    zip = models.TextField(max_length=50)
    country = models.TextField(max_length=50)
    comments = models.TextField(max_length=50,null=True,blank=True)

    def __str__(self):
        return self.company_name


class Item(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField()
    barcode = models.IntegerField(null=True)
    isActive = models.BooleanField(default=True)
    category = models.ForeignKey(Category, null=True)
    stock_count = models.IntegerField()
    supplier = models.ForeignKey(Supplier, null=True)
    created_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.title



