from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import ObjectDoesNotExist
from .forms import ItemForm, CategoryForm ,SupplierForm
from .tables import ItemTable ,CategoryTable ,SupplierTable
from .models import Item, Category ,Supplier
from django.contrib import messages


def index(request):
    return render(request, 'product/create.html')


def create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.title = request.POST.get('title')
            item.description = request.POST.get('description')
            item.save()
            messages.success(request, 'Product Added')
            return redirect('product:manage')
    else:
        form = ItemForm()
        return render(request, 'product/create_form.html', {'form': form})


def manage(request):
    item = ItemTable()
    return render(request, "product/manage.html", {'item': item})


def edit(request, pk):
    item = get_object_or_404(Item, pk=pk)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(commit=False)
            item.title = request.POST.get('title')
            item.description = request.POST.get('description')
            item.save()
            messages.success(request, 'Product Edited')
            return redirect('product:manage')
    else:
        form = ItemForm(instance=item)
        return render(request, 'product/edit.html', {'form': form})


def delete_product(request,pk):
    item = get_object_or_404(Item, pk=pk)
    item.delete()
    messages.success(request, 'Product Deleted')
    return redirect('product:manage')


def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            messages.success(request, 'Category Added')
            return redirect('product:manage_category')
    else:
        form = CategoryForm()
        return render(request, "product/category_create.html", {'form': form})


def edit_category(request,pk):
    item = get_object_or_404(Category, pk=pk)
    if request.method == "POST":
        form = CategoryForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            messages.success(request, 'Category Edited')
            return redirect('product:manage_category')
    else :
        form = CategoryForm(instance=item)
        return render(request, "product/category_edit.html", {'form': form})


def manage_category(request):
    item = CategoryTable()
    return render(request, "product/manage_category.html", {'item': item})


def delete_category(request,pk):
    item = get_object_or_404(Category, pk=pk)
    item.delete()
    messages.success(request, 'Category Deleted')
    return redirect('product:manage_category')


def create_supplier(request):
    if request.method == "POST":
        form = SupplierForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            messages.success(request, 'Supplier Added')
            return redirect('product:manage_supplier')
    else:
        form = SupplierForm()
        return render(request, "product/create_supplier.html", {'form': form})


def edit_supplier(request,pk):
    item = get_object_or_404(Supplier, pk=pk)
    if request.method == "POST":
        form = SupplierForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            messages.success(request, 'Category Edited')
            return redirect('product:manage_supplier')
    else :
        form = SupplierForm(instance=item)
        return render(request, "product/edit_supplier.html", {'form': form})


def manage_supplier(request):
    item = SupplierTable()
    return render(request, "product/manage_supplier.html", {'item': item})


def delete_supplier(request,pk):
    item = get_object_or_404(Supplier, pk=pk)
    item.delete()
    messages.success(request, 'Category Deleted')
    return redirect('product:manage_supplier')