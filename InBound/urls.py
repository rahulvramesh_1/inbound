from django.conf.urls import include,url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from django.contrib.auth import views
from user.forms import LoginForm

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('user.urls')),
    url(r'product/', include('product.urls')),
    url(r'supplier/', include('product.urls')),
    url(r'^login/$', views.login, {'template_name': 'login.html', 'authentication_form': LoginForm},name='login'),  
    url(r'^logout/$', views.logout, {'next_page': '/login'}),  
]


if settings.DEBUG:
    urlpatterns += (static(settings.STATIC_URL, document_root=settings.STATIC_ROOT))