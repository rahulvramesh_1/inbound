#log/forms.py
from django.contrib.auth.forms import AuthenticationForm 
from django import forms

# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Email", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'md-input', 'name': 'email'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'md-input', 'name': 'password'}))